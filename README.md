# terraform

[deploy-vars]: https://confluence.atlassian.com/bitbucket/bitbucket-deployments-940695276.html
[local-setup]: https://ibalce_deloitte@bitbucket.org/ibalce_deloitte/local-setup.git 
[pipelines]: bitbucket-pipelines.yml
[repo-vars]: https://confluence.atlassian.com/bitbucket/environment-variables-in-bitbucket-pipelines-794502608.html

This project creates a docker image based on `golang:alpine` and installs the following tools:
- `make`
- `terraform`

## Variables
The [`bitbucket-pipelines.yml`][pipelines] expects the following [`REPOSITORY_VARIABLES`][repo-vars]

```bash
TERRAFORM_VERSION=0.11.8
```

The following variables are set in `Settings` > `Pipelines` > [`Deployments`][deploy-vars]
```bash
AWS_ACCOUNT_ID=
AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
```


The following variable is set for the account and may be overridden in the [Repository][repo-vars] or [Deployments][deploy-vars] settings.
```bash
AWS_DEFAULT_REGION=eu-west-1
```

# Steps
## Test with `bats`
Install `bats` locally and run the tests
```
brew install bats
chmod a+x test/*.bats
bats test/*.bats
```

## Login to Container Registry
Given you have a configured AWS profile `prd-scorable-god`, login to docker with aws-cli
> Configure your profiles with [local-setup]

``` 
export AWS_PROFILE=prd-scorable-god
export AWS_DEFAULT_REGION=eu-west-1
$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION)
```
alternatively, with `AWS_ACCESS_KEY_ID` & `AWS_SECRET_ACCESS_KEY` and printing out to `login.sh`
``` 
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export AWS_DEFAULT_REGION=eu-west-1
echo $(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION) > login.sh
```

## Create an ECR repository
Create a repository for the docker image i.e. `terraform`
```
aws ecr create-repository --repository-name terraform
```

## Build and push image
```
# Go to where Dockerfile is
export IMAGE=terraform
export VERSION=0.11.8
docker build -t ${IMAGE}:${VERSION} .
docker tag ${IMAGE}:${VERSION} ${IMAGE}:latest
source login.sh && docker push ${IMAGE}
```