#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=$(basename $(pwd))

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "GnuMake installed" {
    run docker run -t ${DOCKER_IMAGE}:test \
        make --version

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}
@test "Terraform installed" {
    run docker run -t ${DOCKER_IMAGE}:test \
        terraform --version

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}
@test "Dummy command throws error" {
    run docker run -t ${DOCKER_IMAGE}:test \
        foo

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -ne 0 ] # expect error
    [ "$output" = 'docker: Error response from daemon: OCI runtime create failed: container_linux.go:344: starting container process caused "exec: \"foo\": executable file not found in $PATH": unknown.' ]
}